# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]>>> '

# Dotfiles Management
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree="$HOME""

alias ls="ls --color=auto"
alias ll="ls --color=auto"

# Git Aliases
alias gclone="git clone --depth=1"
alias ggin="git remote add origin"
alias gpush="git push -u origin master"

