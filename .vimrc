" Enable Number "
set number
" set relativenumber

" Enable Type File Detection "
filetype on

" Enable plugins and load plugins for the detected file "
filetype plugin on

" Load indentation for the detected file type " 
filetype indent on

" turn on syntax highlighting "
syntax on

" Cursor highlighting "
set cursorline

set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab
set nobackup
set scrolloff=10
set nowrap
set incsearch
set ignorecase
set smartcase
set showcmd
set noshowmode
set showmatch
set hlsearch
set history=1000

set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
