(define-package "evil-collection" "20220417.1119" "A set of keybindings for Evil mode"
  '((emacs "25.1")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "63c6fd00c23b91e6b84b735add756d6e4b5c3b86" :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :keywords
  '("evil" "tools")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
