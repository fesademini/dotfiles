; enabling melpa and elpa repository
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("elpa" . "https://elpa.gnu.org/packages/")))

; Bootstrapping use-package
(package-initialize)
(setq use-package-always-ensure t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

;;; UNDO
  ; Vim style undo not needed for emacs 28
  (use-package undo-fu)

;;; Vim Bindings
(use-package evil
             :demand t
             :bind (("<escape>" . keyboard-escape-quit))
             :init
             (setq evil-want-keybinding nil)
             (setq evil-undo-system 'undo-fu)
             :config
             (evil-mode 1))

;;; Vim Bindings everywhere else
(use-package evil-collection
             :after evil
             :config
             (setq evil-want-integration t)
             (evil-collection-init))

;;; Solarized Theme
(use-package solarized-theme)

;;; Disabling menubar, toolbar, scrollbar
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)

;;; Enable numberline
(global-linum-mode 1)

;;; Settings Color Scheme
(load-theme 'solarized-dark t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(evil-collection evil undo-fu use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
